const db = require('./db')

const Sequelize = db.Sequelize;
const sequelize = db.sequelize;

// User model
const User = sequelize.define('user', {
    id: { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV4, primaryKey: true },
    studentNumber: { type: Sequelize.STRING(8) },
    cardUUID: { type: Sequelize.STRING(12), unique: true },
    name: { type: Sequelize.STRING(54) },
    surname: { type: Sequelize.STRING(54) },
    balance: { type: Sequelize.DOUBLE }
});

// Searchable fields
User.searchFields = (regex) => {
    return [{name: regex}, {surname: regex}, {studentNumber: regex}];
}

module.exports = User