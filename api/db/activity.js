const db = require('./db')

const Sequelize = db.Sequelize;
const sequelize = db.sequelize;

// Model
const Activity = sequelize.define('activity', {
    id: { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV4, primaryKey: true },
    date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    description: { type: Sequelize.STRING }
});

// Searchable fields
Activity.searchFields = (regex) => {
    return [ { description: regex } ];
}

module.exports = Activity