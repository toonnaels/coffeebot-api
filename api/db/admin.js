const db = require('./db')

const Sequelize = db.Sequelize;
const sequelize = db.sequelize;

// Model
const Admin = sequelize.define('admin', {
    id: { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV4, primaryKey: true },
    email: { type: Sequelize.STRING, unique: true },
    password: { type: Sequelize.STRING, unique: true },
    name: { type: Sequelize.STRING(54) },
    surname: { type: Sequelize.STRING(54) }
});

// Searchable fields
Admin.searchFields = (regex) => {
    return [];
}

module.exports = Admin