const db = require('./db')

const User = require('./user')
const CoffeeOption = require('./coffeeOption')

const Sequelize = db.Sequelize;
const sequelize = db.sequelize;

// Model
const Purchase = sequelize.define('purchase', {
    id: { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV1, primaryKey: true },
    userId: { type: Sequelize.UUID, references: { model: User, key: 'id' } },
    itemId: { type: Sequelize.UUID, references: { model: CoffeeOption, key: 'id' } },
    date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW }
});

Purchase.belongsTo(User);
Purchase.belongsTo(CoffeeOption, {as: "item"});

// Searchable fields
Purchase.searchFields = (regex) => {
    return [ ];
}

module.exports = Purchase