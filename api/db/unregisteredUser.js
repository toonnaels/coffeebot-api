const db = require('./db')

const Sequelize = db.Sequelize;
const sequelize = db.sequelize;

// Model
const UnregisteredUser = sequelize.define('unregisteredUser', {
    id: { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV1, primaryKey: true },
    cardUUID: { type: Sequelize.STRING(12) }
});

// Searchable fields
UnregisteredUser.searchFields = (regex) => {
    return [{cardUUID: regex}];
}

module.exports = UnregisteredUser