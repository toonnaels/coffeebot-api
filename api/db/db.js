const Sequelize = require('sequelize');

// Connection URL.
const db_host = process.env.DB_HOST || "localhost"
const db_port = process.env.DB_PORT || "3306"
const db_auth_username = process.env.DB_INITDB_ROOT_USERNAME || "root";
const db_auth_password = process.env.DB_INITDB_ROOT_PASSWORD || null;
const db_name = `coffeebot`;

const sequelize = new Sequelize(db_name, db_auth_username, db_auth_password, {
    // DB
    host: db_host,
    port: db_port,
    dialect: 'mysql',
    operatorsAliases: false,

    // Tables
    define: {
        timestamps: false,
        force: false
    }
});

function connect() {
    function retryForeverWithBackoff(operation, delay) {
        return operation(). // run the operation
            catch(_ => { // if it fails
                console.log(`Failed to connect to mariadb on startup - retrying in about ${Math.ceil(delay/1000)} seconds.`);
                return new Promise(r => setTimeout(r, delay)). // delay 
                   // retry with more time
                   then(retryForeverWithBackoff.bind(null, operation, delay * 1.2)); 
            });
    }
    

    var connectWithRetry = function() {
        return sequelize.authenticate()
            .then(() => {
                console.log('Connection to mariadb has been established successfully.');
            })
    }

    // Connect to DB
    return retryForeverWithBackoff(connectWithRetry, 2000);
}

exports.connect = connect;
exports.Sequelize = Sequelize;
exports.sequelize = sequelize;