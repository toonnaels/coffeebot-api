const db = require('./db')

const Sequelize = db.Sequelize;
const sequelize = db.sequelize;

// Model
const CoffeeOption = sequelize.define('coffeeOption', {
    id: { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV4, primaryKey: true },
    item: { type: Sequelize.STRING(20), validate: { isLowercase: true }, unique: true },
    price: { type: Sequelize.DOUBLE }
});

// Searchable fields
CoffeeOption.searchFields = (regex) => {
    return [{item: regex}];
}

module.exports = CoffeeOption