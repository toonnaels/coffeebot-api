class AggregationHelpers {
    static aggregationLookupId(fromModel, localField , asField) {
        return { $lookup: { from: fromModel.prototype.collection.name, localField: localField, foreignField: '_id', as: asField } };
    }
    
    static aggregationUnwind(unwindField) {
        return { $unwind: '$' + unwindField };
    }
    
    static aggregationMatch(matchObj) {
        return { $match: matchObj };
    }
    
    static aggregationOr(orArr) {
        return { $or: orArr };
    }
    
    static aggregationRegex(field, filterString) {
        var regexObj = new Object;
        regexObj[field] = { $regex: filterString, $options: 'i' };
        return regexObj;
    }
    
    static aggregationSort(sortField) {
        var sortDir = 1;
        var sortString = sortField;
        if(sortField.charAt(0) === '-') {
            sortDir = -1;
            sortString = sortField.substr(1);
        }
    
        var sortObj = new Object;
        sortObj.$sort = {};
        sortObj.$sort[sortString] = sortDir;
        return sortObj;
    }
    
    static aggregationPaginate(page, pageSize) {
        const skip = (page) * pageSize;
        const limit = pageSize; 
    
        return [
            { $group: { _id: null, totalRecords: { $sum: 1 }, response: { $push: '$$ROOT' } } },
            { $addFields: { page: (page + 1) } },
            { $addFields: { totalPages: { $ceil: { $divide: ["$totalRecords", pageSize] } } } },
            { $project: { _id: 0, totalRecords: 1, page: 1, totalPages: 1, response: { $slice: ['$response', skip, limit] } } }
        ];
    }
}

module.exports =  AggregationHelpers;