const db = require('../db/db');
const Sequelize = db.Sequelize;

function getAll(filter, sort, page, pageSize, response, errorCallback, Model) {
    if(page <= 0 || pageSize <= 0) {
        errorCallback({status: 400, error: `The page and pageSize parameters must be positive.`});
        return;
    }

    let findOptions = {};
    if(filter && Model.searchFields({}).length > 0) {
        const regex = { [Sequelize.Op.regexp]: `.*${filter.replace(',', '|')}.*` };
        findOptions = { [Sequelize.Op.or]: Model.searchFields(regex) };
    }

    if(sort) {
        if(page && pageSize) {
            const offsetVal = (+page - 1) * +pageSize;
            Model.findAndCountAll({ order: sortToOrder(sort), offset: offsetVal, limit: +pageSize, where: findOptions }).then(result => safePaginatedExecDbQuery(result.rows, page, pageSize, result.count)).catch(err => errorCallback(err));
        } else {
            Model.findAll({ order: sortToOrder(sort), where: findOptions }).then(models => execDbQuery(models, 1, 1, 0, false)).catch(err => errorCallback(err));
        }
    } else if(page && pageSize) {
        const offsetVal = (+page - 1) * +pageSize;
        Model.findAndCountAll({ offset: offsetVal, limit: +pageSize, where: findOptions }).then(result => safePaginatedExecDbQuery(result.rows, page, pageSize, result.count)).catch(err => errorCallback(err));
    } else {
        Model.findAll({ where: findOptions }).then(models => execDbQuery(models, 1, 1, 0, false)).catch(err => errorCallback(err));
    }

    function sortToOrder(sort) {
        if(sort.charAt(0) == '-') {
            const col = sort.substring(1);
            return [[ col, 'DESC' ]];
        } else {
            return [[ sort ]];
        }
    }

    function safePaginatedExecDbQuery(models, page, pageSize, total) {
        if(!models) {
            execDbQuery([], 0, 0, 0, true);
        } else {
            execDbQuery(models, page, pageSize, total, true);
        }
    }

    function execDbQuery(models, page, pageSize, totalRecords, paginated) {
    if(paginated) {
            response({ page: page, totalPages: Math.ceil(totalRecords / pageSize), totalRecords: totalRecords, response: models })
        } else {
            response(models);
        }
    }
}

function getById(id, response, errorCallback, Model, modelName) {
    Model.findById(id)
        .then(model => response(model))
        .catch(_ => errorCallback({status: 404, error: `Could not find ${modelName} with id ${id}`}));
}

function save(response, errorCallback, model, modelName, Model) {
    Model.create(model)
        .then(model => response(model))
        .catch(err => errorCallback({status: 400, error: `Failed to save the ${modelName}`}));
}

function update(id, model, response, errorCallback, Model, modelName) {
    Model.findById(id)
        .then(currentModel => {
            Model.update(model, { where: { id: currentModel.id } })
                .then(_ => response(model))
                .catch(err => errorCallback({status: 400, error: `Failed to update ${modelName} with id ${id}`}));
        })
        .catch(_ => errorCallback({status: 400, error: `No ${modelName} with id ${id} exists.`}));
}

function deleteById(id, response, errorCallback, Model, modelName) {
    Model.findById(id)
        .then(currentModel => {
            Model.destroy({ where: { id: currentModel.id } })
                .then(() => response())
                .catch(err => errorCallback({status: 400, error: `Failed to remove ${modelName} with id ${id} ${err}`}));
        })
        .catch(_ => errorCallback({status: 400, error: `No ${modelName} with id ${id} exists.`}));
}

exports.getAll = getAll;
exports.getById = getById;
exports.save = save;
exports.update = update;
exports.deleteById = deleteById;