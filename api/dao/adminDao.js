const commonDao = require('./commonDao');
const Admin = require('../db/admin');

// Defined only for re-use purposes.
const Model = Admin;
const modelString = "admin";

// Common functions.
function getAll(filter, sort, page, pageSize, response, errorCallback) {
    commonDao.getAll(filter, sort, page, pageSize, response, errorCallback, Model);
}
function getById(id, response, errorCallback) {
    commonDao.getById(id, response, errorCallback, Model, modelString);
}
function save(response, errorCallback, model) {
    commonDao.save(response, errorCallback, model, modelString, Model);
}
function update(id, model, response, errorCallback) {
    commonDao.update(id, model, response, errorCallback, Model, modelString);
}
function deleteById(id, response, errorCallback) {
    commonDao.deleteById(id, response, errorCallback, Model, modelString);
}

exports.getAll = getAll;
exports.getById = getById;
exports.save = save;
exports.update = update;
exports.deleteById = deleteById;

// Specific functions.
function getAdminByQuery(query, exec) {
    Admin.findOne({ where: query })
        .then(model => {
            if(!model) {
                exec("Failed to fetch admin.", undefined);
            } else {
                exec(undefined, model.dataValues)
            }
        })
        .catch(err => exec(err, undefined));
}

function authenticateAdmin(response, errorCallback, credentials) {
    Admin.findOne({ where: credentials })
        .then(admin => response(admin))
        .catch(err => errorCallback(err));
}

exports.getAdminByQuery = getAdminByQuery;
exports.authenticateAdmin = authenticateAdmin;