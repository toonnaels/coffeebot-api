const commonDao = require('./commonDao');
const unregisteredUserDao = require('./unregisteredUserDao');
const activityDao = require('./activityDao');

const User = require('../db/user');
const CoffeeOption = require('../db/coffeeOption');
const Purchase = require('../db/purchase');
const UnregisteredUser = require('../db/unregisteredUser');

const db = require('../db/db');

const Sequelize = db.Sequelize;
const sequelize = db.sequelize;

// Defined only for re-use purposes.
const Model = User;
const modelString = "user";

// Common functions.
function getAll(filter, sort, page, pageSize, response, errorCallback) {
    commonDao.getAll(filter, sort, page, pageSize, response, errorCallback, Model);
}
function getById(id, response, errorCallback) {
    commonDao.getById(id, response, errorCallback, Model, modelString);
}
function save(response, errorCallback, model) {
    commonDao.save((respModel) => logActivity(`New user, ${model.name} ${model.surname}, created. Welcome to the ESL!`, () => response(respModel), errorCallback), errorCallback, model, modelString, Model);
}
function update(id, model, response, errorCallback) {
    commonDao.update(id, model, response, errorCallback, Model, modelString);
}
function deleteById(id, response, errorCallback) {
    // First delete all purchases
    Purchase.destroy({ where: { userId: id } })
        .then(_ => getById(id, (respModel) => logActivity(`User, ${respModel.name} ${respModel.surname}, deleted. We'll miss you!`, () => commonDao.deleteById(id, response, errorCallback, Model, modelString), errorCallback), errorCallback))
        .catch(_ => errorCallback)
}

// Log activity.
function logActivity(description, response, errorCallback) {
    const activity = { description: description };

    activityDao.save(response, errorCallback, activity);
}

exports.getAll = getAll;
exports.getById = getById;
exports.save = save;
exports.update = update;
exports.deleteById = deleteById;

// Specific functions.
function registerUser(response, errorCallback, model) {
    UnregisteredUser.findOne({ where: {cardUUID: model.cardUUID }})
        .then(unregModel => {
            save((respModel) => deleteUnregisteredUser(unregModel, () => response(respModel)), errorCallback, model);

            function deleteUnregisteredUser(unregModel, next) {
                unregisteredUserDao.deleteById(unregModel.id, next, errorCallback);
            }
        })
        .catch(_ => errorCallback({status: 404, error: `Failed to fetch unregistered user with card UUID ${model.cardUUID}.`}));
}

function getTopDrinkers(response, errorCallback) {
    getTopDrinkersByType('coffee', response, errorCallback);
}

function getTopMilk(response, errorCallback) {
    getTopDrinkersByType('milk', response, errorCallback);
}

function getTopDrinkersByType(drinkType, response, errorCallback) {
    const now = new Date();
    const day = new Date(now.getFullYear(), now.getMonth(), now.getDate());
    const week = getMonday(now.toDateString());
    const month = new Date(now.getFullYear(), now.getMonth());
    const year = new Date(now.getFullYear(), 0);

    getTopDrinkersByDate(drinkType, year).then(yearResult => {
        getTopDrinkersByDate(drinkType, month).then(monthResult => {
            getTopDrinkersByDate(drinkType, week).then(weekResult => {
                getTopDrinkersByDate(drinkType, day).then(dayResult => {
                    response({
                        day: dayResult,
                        week: weekResult,
                        month: monthResult,
                        year: yearResult
                    });
                }).catch(err => errorCallback(err));
            }).catch(err => errorCallback(err));
        }).catch(err => errorCallback(err));
    }).catch(err => errorCallback(err));
}

function getTopDebtors(response, errorCallback) {
    User.findAll({ order: ['balance'],  where: { balance: { [Sequelize.Op.lt]: 0 }}, limit: 3})
        .then(debtors => response({ users: debtors }))
        .catch(err => errorCallback(err));
}

function getTopPayers(response, errorCallback) {
    User.findAll({ order: [['balance', 'DESC']],  where: { balance: { [Sequelize.Op.gt]: 0 }}, limit: 3})
        .then(payers => response({ users: payers }))
        .catch(err => errorCallback(err));
}

function getUserByQuery(query, exec) {
    User.findOne({ where: query })
        .then(result => exec(undefined, result))
        .catch(err => exec(err, undefined));
}

function loadCredit(response, errorCallback, model) {
    if(model.id && model.amount && !isNaN(model.amount)) {
        User.findById(model.id)
            .then(user => {
                user.balance = user.balance + model.amount;
                update(model.id, user.dataValues, (respModel) => logActivity(`Loaded R${parseFloat(Math.round(model.amount * 100) / 100).toFixed(2)} for ${user.name} ${user.surname}.`, () => response(respModel), errorCallback), errorCallback);
            })
            .catch(_ => errorCallback({status: 404, error: `The user with id ${model.id} does not exist.`}));
    } else {
        errorCallback({status: 400, error: `The requested object is invalid. Should consist of an user id and the load credit amount. Request: ${model}`});
    }
}

// Private functions.
function getTopDrinkersByDate(drinkType, fromDate) {
    if(!fromDate) {
        fromDate = new Date().setTime(0);
    }

    return Purchase.findAll({
        attributes: ['userId', [sequelize.fn('count', sequelize.col('userId')), 'purchases']],
        include: [{ model: User }, { model: CoffeeOption, as: 'item', where: { item: { [Sequelize.Op.like]: [`%${drinkType}%`]}} }],
        where: { date: { [Sequelize.Op.gte]: fromDate }},
        group: 'userId',
        order: [[sequelize.literal('purchases'), 'DESC']],
        limit: 3 });
}

function getMonday(d) {
    d = new Date(d);
    const day = d.getDay();
    const diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
    return new Date(d.setDate(diff));
}

exports.registerUser = registerUser;
exports.getTopDrinkers = getTopDrinkers;
exports.getTopMilk = getTopMilk;
exports.getTopDebtors = getTopDebtors;
exports.getTopPayers = getTopPayers;
exports.getUserByQuery = getUserByQuery;
exports.loadCredit = loadCredit;