const commonDao = require('./commonDao');
const CoffeeOption = require('../db/coffeeOption');

// Defined only for re-use purposes.
const Model = CoffeeOption;
const modelString = "coffee option";

// Common functions.
function getAll(filter, sort, page, pageSize, response, errorCallback) {
    commonDao.getAll(filter, sort, page, pageSize, response, errorCallback, Model);
}
function getById(id, response, errorCallback) {
    commonDao.getById(id, response, errorCallback, Model, modelString);
}
function save(response, errorCallback, model) {
    commonDao.save(response, errorCallback, model, modelString, Model);
}
function update(id, model, response, errorCallback) {
    commonDao.update(id, model, response, errorCallback, Model, modelString);
}
function deleteById(id, response, errorCallback) {
    commonDao.deleteById(id, response, errorCallback, Model, modelString);
}

exports.getAll = getAll;
exports.getById = getById;
exports.save = save;
exports.update = update;
exports.deleteById = deleteById;

// Specific functions
function getByQuery(query, exec) {
    CoffeeOption.findOne({ where: query })
        .then(result => exec(undefined, result))
        .catch(err => exec(err, undefined));
}

exports.getByQuery = getByQuery;