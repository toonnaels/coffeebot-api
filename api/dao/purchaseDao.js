const commonDao = require('./commonDao');
const Purchase = require('../db/purchase');
const User = require('../db/user');
const CoffeeOption = require('../db/coffeeOption');

const db = require('../db/db');

const sequelize = db.sequelize;
const Sequelize = db.Sequelize;

// Defined only for re-use purposes.
const Model = Purchase;
const modelString = "purchase";

// Common functions.
function getAll(filter, sort, page, pageSize, response, errorCallback) {
    commonDao.getAll(filter, sort, page, pageSize, response, errorCallback, Model);
}
function getById(id, response, errorCallback) {
    commonDao.getById(id, response, errorCallback, Model, modelString);
}
function save(response, errorCallback, model) {
    commonDao.save(response, errorCallback, model, modelString, Model);
}
function update(id, model, response, errorCallback) {
    commonDao.update(id, model, response, errorCallback, Model, modelString);
}
function deleteById(id, response, errorCallback) {
    commonDao.deleteById(id, response, errorCallback, Model, modelString);
}

exports.getAll = getAll;
exports.getById = getById;
exports.save = save;
exports.update = update;
exports.deleteById = deleteById;

// Specific functions.
function getAllNestedData(filter, sort, page, pageSize, response, errorCallback) {
    if(page <= 0 || pageSize <= 0) {
        errorCallback({status: 400, error: `The page and pageSize parameters must be positive.`});
        return;
    }

    const userField = 'user';
    const itemField = 'coffeeOption';

    let findOptions = {};
    if(filter) {
        const regex = { [Sequelize.Op.regexp]: `.*${filter.replace(',', '|')}.*` };

        // Add 'user.' to fields.
        const userSearchTemp = User.searchFields(regex);
        const userSearchArr = [];
        for(var i in userSearchTemp) {
            const userSearchElem = {};
            for(var prop in userSearchTemp[i]) {
                userSearchElem['$user.' + prop + '$'] = userSearchTemp[i][prop];
            }
            userSearchArr[i] = userSearchElem;
        }

        // Add 'coffeeOption.' to fields.
        const coffeeOptionSearchTemp = CoffeeOption.searchFields(regex);
        const coffeeOptionSearchArr = [];
        for(var i in coffeeOptionSearchTemp) {
            const coffeeOptionSearchElem = {};
            for(var prop in coffeeOptionSearchTemp[i]) {
                coffeeOptionSearchElem['$item.' + prop + '$'] = coffeeOptionSearchTemp[i][prop];
            }
            coffeeOptionSearchArr[i] = coffeeOptionSearchElem;
        }
        
        findOptions = { [Sequelize.Op.or]: userSearchArr.concat(coffeeOptionSearchArr) };
    }

    if(sort) {
        if(page && pageSize) {
            const offsetVal = (+page - 1) * +pageSize;
            Purchase.findAndCountAll({ order: sortToOrder(sort), offset: offsetVal, limit: +pageSize, where: findOptions, include: [{ all: true }] }).then(result => safePaginatedExecDbQuery(result.rows, page, pageSize, result.count)).catch(err => errorCallback(err));
        } else {
            Purchase.findAll({ order: sortToOrder(sort), where: findOptions, include: [{ all: true }] }).then(models => execDbQuery(models, 1, 1, 0, false)).catch(err => errorCallback(err));
        }
    } else if(page && pageSize) {
        const offsetVal = (+page - 1) * +pageSize;
        Purchase.findAndCountAll({ offset: offsetVal, limit: +pageSize, where: findOptions, include: [{ all: true }] }).then(result => safePaginatedExecDbQuery(result.rows, page, pageSize, result.count)).catch(err => errorCallback(err));
    } else {
        Purchase.findAll({ where: findOptions, include: [{ all: true }] }).then(models => execDbQuery(models, 1, 1, 0, false)).catch(err => errorCallback(err));
    }

    function getSortTable(sort, userField, itemField) {
        var sortString = sort;
     
        if(sortString.charAt(0) === '-') {
            sortString = sort.substring(1);
        }
    
        if(sortString === 'name' || sortString === 'surname' || sortString === 'studentNumber' || sortString === 'cardUUID' || sortString === 'balance') {
            return 'purchases.'.concat(userField);
        } else if(sortString === 'item' || sortString === 'price') {
            return 'purchases.'.concat(itemField);
        } else {
            return sort;
        }
    }

    function sortToOrder(sort) {
        if(sort.charAt(0) == '-') {
            const col = sort.substring(1);
            return [[ sequelize.col(col, getSortTable(col, userField, itemField)), 'DESC' ]];
        } else {
            return [[ sequelize.col(sort, getSortTable(sort, userField, itemField)) ]];
        }
    }

    function safePaginatedExecDbQuery(models, page, pageSize, total) {
        if(!models) {
            execDbQuery([], 0, 0, 0, true);
        } else {
            execDbQuery(models, page, pageSize, total, true);
        }
    }

    function execDbQuery(models, page, pageSize, totalRecords, paginated) {
    if(paginated) {
            response({ page: page, totalPages: Math.ceil(totalRecords / pageSize), totalRecords: totalRecords, response: models })
        } else {
            response(models);
        }
    }
}

function getSummary(fromDate, toDate, response, errorCallback) {
    Purchase.findAll({
        attributes: ['itemId', [sequelize.fn('count', sequelize.col('itemId')), 'purchased']],
        include: [{ model: CoffeeOption, as: 'item' }],
        where: { date: { [Sequelize.Op.gte]: fromDate, [Sequelize.Op.lte]: toDate }},
        group: 'itemId',
        order: [[sequelize.literal('purchased'), 'DESC']] })
        .then(purchases => {
            response(purchases)
        })
        .catch(err => errorCallback({status: 404, error: `Failed to fetch purchase summary. Reason: ${err}`}));
}

exports.getAllNestedData = getAllNestedData;
exports.getSummary = getSummary;