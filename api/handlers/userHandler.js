const commonHandler = require('./commonHandler');
const userDao = require('../dao/userDao');
const purchaseDao = require('../dao/purchaseDao');
const coffeeOptionDao = require('../dao/coffeeOptionDao');

// Defined only for re-use purposes.
const dao = userDao;

// Common functions.
function getAll(req, res, next) {
    commonHandler.getAll(req.query.filter, req.query.sort, req.query.page, req.query.pageSize, res, next, dao);
}
function getById(req, res, next) {
    commonHandler.getById(req, res, next, dao);
}
function save(req, res, next) {
    commonHandler.save(res, next, dao, req.body);
}
function update(req, res, next) {
    commonHandler.update(req, res, next, dao);
}
function deleteById(req, res, next) {
    commonHandler.deleteById(req, res, next, dao);
}

exports.getAll = getAll;
exports.getById = getById;
exports.save = save;
exports.update = update;
exports.deleteById = deleteById;

// Specific functions.
function registerUser(req, res, next) {
    userDao.registerUser(res.json.bind(res), next, req.body);
}

function getTopDrinkers(req, res, next) {
    userDao.getTopDrinkers(res.json.bind(res), next);
}

function getTopMilk(req, res, next) {
    userDao.getTopMilk(res.json.bind(res), next);
}

function getTopDebtors(req, res, next) {
    userDao.getTopDebtors(res.json.bind(res), next);
}

function getTopPayers(req, res, next) {
    userDao.getTopPayers(res.json.bind(res), next);
}

function getUserByStudentNumber(req, res, next) {
    const studentNumber = req.params.studentNumber;
    userDao.getUserByQuery({ studentNumber: studentNumber }, (err, user) => {
        if(err || !user) {
            next({status: 404, error: `Could not find user with studentNumber ${studentNumber}`});
        } else {
            res.json(user);
        }
    });
}

function getUserByCardId(req, res, next) {
    const cardId = req.params.cardId;
    userDao.getUserByQuery({ cardUUID: cardId }, (err, user) => {
        if(err || !user) {
            next({status: 404, error: `Could not find user with cardUUID ${cardId}`});
        } else {
            res.json(user);
        }
    });
}

function loadCredit(req, res, next) {
    userDao.loadCredit(res.json.bind(res), next, req.body);
}

function purchase(req, res, next) {
    const userId = req.params.userId
    const itemDesc = req.params.item
    coffeeOptionDao.getByQuery({ item:  itemDesc }, (err, item) => {
        if(err || !item) {
            next({status: 404, error: `Could not find item with description ${itemDesc} ${err}`});
        } else {
            function getUser(purchase) {
                userDao.getById(userId, userPurchase, next);
            }

            function userPurchase(user) {
                user.dataValues.balance -= item.price;
                userDao.update(userId, user.dataValues, res.json(user.dataValues), next);
            }

            purchaseDao.save(getUser, next, { userId: userId, itemId: item.id });
        }
    });
}

exports.registerUser = registerUser;
exports.getTopDrinkers = getTopDrinkers;
exports.getTopMilk = getTopMilk;
exports.getTopDebtors = getTopDebtors;
exports.getTopPayers = getTopPayers;
exports.getUserByStudentNumber = getUserByStudentNumber;
exports.getUserByCardId = getUserByCardId;
exports.loadCredit = loadCredit;
exports.purchase = purchase;