function getAll(filter, sort, page, pageSize, res, next, dao) {
    dao.getAll(filter, sort, page, pageSize, res.json.bind(res), next);
}

function getById(req, res, next, dao) {
    dao.getById(req.params.id, res.json.bind(res), next);
}

function save(res, next, dao, newModel) {
    dao.save(res.status(201).send.bind(res), next, newModel);
}

function update(req, res, next, dao) {
    dao.update(req.params.id, req.body, res.json.bind(res), next);
}

function deleteById(req, res, next, dao) {
    dao.deleteById(req.params.id, res.status(204).send.bind(res), next);
}

exports.getAll = getAll;
exports.getById = getById;
exports.save = save;
exports.update = update;
exports.deleteById = deleteById;