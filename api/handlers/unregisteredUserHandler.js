const commonHandler = require('./commonHandler');
const unregisteredUserDao = require('../dao/unregisteredUserDao');

// Defined only for re-use purposes.
const dao = unregisteredUserDao;

// Common functions.
function getAll(req, res, next) {
    commonHandler.getAll(req.query.filter, req.query.sort, req.query.page, req.query.pageSize, res, next, dao);
}
function getById(req, res, next) {
    commonHandler.getById(req, res, next, dao);
}
function save(req, res, next) {
    commonHandler.save(res, next, dao, req.body);
}
function update(req, res, next) {
    commonHandler.update(req, res, next, dao);
}
function deleteById(req, res, next) {
    commonHandler.deleteById(req, res, next, dao);
}

exports.getAll = getAll;
exports.getById = getById;
exports.save = save;
exports.update = update;
exports.deleteById = deleteById;

// Specific functions
function getByCardId(req, res, next) {
    const cardId = req.params.cardId;
    unregisteredUserDao.getByQuery({ cardUUID: cardId }, (err, user) => {
        if(err || !user) {
            next({status: 404, error: `Could not find unregistered user with cardUUID ${cardId}`});
        } else {
            res.json(user);
        }
    });
}

function newEntry(req, res, next) {
    const cardId = req.params.cardId;
    unregisteredUserDao.getByQuery({ cardUUID: cardId }, (err, user) => {
        if(err || !user) {
            unregisteredUserDao.save(res.json.bind(res), next, { cardUUID: cardId });
        } else {
            res.json(user);
        }
    });
}

exports.getByCardId = getByCardId;
exports.newEntry = newEntry;