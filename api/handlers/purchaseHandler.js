const commonHandler = require('./commonHandler');
const purchaseDao = require('../dao/purchaseDao');

// Defined only for re-use purposes.
const dao = purchaseDao;

// Common functions.
function getAll(req, res, next) {
    commonHandler.getAll(req.query.filter, req.query.sort, req.query.page, req.query.pageSize, res, next, dao);
}
function getById(req, res, next) {
    commonHandler.getById(req, res, next, dao);
}
function save(req, res, next) {
    commonHandler.save(res, next, dao, req.body);
}
function update(req, res, next) {
    commonHandler.update(req, res, next, dao);
}
function deleteById(req, res, next) {
    commonHandler.deleteById(req, res, next, dao);
}

exports.getAll = getAll;
exports.getById = getById;
exports.save = save;
exports.update = update;
exports.deleteById = deleteById;

// Specific functions.
function getAllNestedData(req, res, next) {
    purchaseDao.getAllNestedData(req.query.filter, req.query.sort, req.query.page, req.query.pageSize, res.json.bind(res), next);
}

function getSummary(req, res, next) {
    purchaseDao.getSummary(req.params.fromDate, req.params.toDate, res.json.bind(res), next);
}

exports.getAllNestedData = getAllNestedData;
exports.getSummary = getSummary;