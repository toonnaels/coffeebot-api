const commonHandler = require('./commonHandler');
const activityDao = require('../dao/activityDao');

// Defined only for re-use purposes.
const dao = activityDao;

// Common functions.
function getAll(req, res, next) {
    commonHandler.getAll(req.query.filter, req.query.sort, req.query.page, req.query.pageSize, res, next, dao);
}
function getById(req, res, next) {
    commonHandler.getById(req, res, next, dao);
}
function save(req, res, next) {
    commonHandler.save(res, next, dao, req.body);
}
function update(req, res, next) {
    commonHandler.update(req, res, next, dao);
}
function deleteById(req, res, next) {
    commonHandler.deleteById(req, res, next, dao);
}

exports.getAll = getAll;
exports.getById = getById;
exports.save = save;
exports.update = update;
exports.deleteById = deleteById;