const commonHandler = require('./commonHandler');
const adminDao = require('../dao/adminDao');

const passportStrategies = require('../server/passport-strategies');
const jwt = require('jsonwebtoken');

// Defined only for re-use purposes.
const dao = adminDao;

// Common functions.
function getAll(req, res, next) {
    commonHandler.getAll(req.query.filter, req.query.sort, req.query.page, req.query.pageSize, res, next, dao);
}
function getById(req, res, next) {
    commonHandler.getById(req, res, next, dao);
}
function save(req, res, next) {
    commonHandler.save(res, next, dao, req.body);
}
function update(req, res, next) {
    commonHandler.update(req, res, next, dao);
}
function deleteById(req, res, next) {
    commonHandler.deleteById(req, res, next, dao);
}

exports.getAll = getAll;
exports.getById = getById;
exports.save = save;
exports.update = update;
exports.deleteById = deleteById;

// Specific functions.
function authenticateAdmin(req, res, next) {
    const credentials = { email: req.body.email, password: req.body.password };
    adminDao.authenticateAdmin(response, next, credentials);

    function response(admin) {
        if(admin) {
            const token = jwt.sign(admin.toJSON(), passportStrategies.secret, {
                expiresIn: 60 * 60 * 24 // 1 Day
            });

            res.json({
                authenticated: true,
                message: "Successfully authenticated.",
                token: token,
                admin: {
                    id: admin.id,
                    name: admin.name,
                    surname: admin.surname,
                    email: admin.email
                }});
        } else {
            res.json({authenticated: false, message: "The email or password is incorrect.", token: '', admin: {}});
        }
    }
}

function changePassword(req, res, next) {
    let adminId = req.params.id;
    let oldpassword = req.body.oldpassword;

    adminDao.getAdminByQuery({id: adminId, password: oldpassword}, updatePassword);

    function updatePassword(err, admin) {
        if(err || !admin) {
            res.json({status: 404, error: `The password is invalid. ${admin}`});
        } else {
            admin.password = req.body.newpassword;
            adminDao.update(adminId, admin, response, next);

            function response(updatedAdmin) {
                if(updatedAdmin) {
                    res.json({updated: true});
                } else {
                    res.json({updated: false});
                }
            }
        }
    }
}

exports.authenticateAdmin = authenticateAdmin;
exports.changePassword = changePassword;