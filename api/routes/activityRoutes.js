const express = require('express');
const router = express.Router();

const commonRoutes = require('./commonRoutes');
const handler = require('../handlers/activityHandler');

commonRoutes.getCommonRoutes(router, handler, false);

module.exports = router;