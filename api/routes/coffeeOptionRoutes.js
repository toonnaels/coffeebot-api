const express = require('express');
const router = express.Router();

const commonRoutes = require('./commonRoutes');
const handler = require('../handlers/coffeeOptionHandler');

commonRoutes.getCommonRoutes(router, handler, false);

module.exports = router;