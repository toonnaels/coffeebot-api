const passport = require('passport');

function getCommonRoutes(router, handler, shouldAuthGet) {
    if(shouldAuthGet) {
        router.get('/', passport.authenticate('jwt', { session: true }), handler.getAll);
        router.get('/:id', passport.authenticate('jwt', { session: true }), handler.getById);
    } else {
        router.get('/', handler.getAll);
        router.get('/:id', handler.getById);
    }

    router.post('/', passport.authenticate('jwt', { session: true }), handler.save);

    router.put('/:id', passport.authenticate('jwt', { session: true }), handler.update);
    router.patch('/:id', passport.authenticate('jwt', { session: true }), handler.update);

    router.delete('/:id', passport.authenticate('jwt', { session: true }), handler.deleteById);
}

exports.getCommonRoutes = getCommonRoutes;