const express = require('express');
const router = express.Router();

const commonRoutes = require('./commonRoutes');
const handler = require('../handlers/unregisteredUserHandler');

router.get('/cardId/:cardId', handler.getByCardId);
router.get('/register/:cardId', handler.newEntry);

commonRoutes.getCommonRoutes(router, handler, false);

module.exports = router;