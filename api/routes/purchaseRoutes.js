const express = require('express');
const router = express.Router();

const commonRoutes = require('./commonRoutes');
const handler = require('../handlers/purchaseHandler');

router.get('/populate', handler.getAllNestedData);
router.get('/summary/:fromDate/:toDate', handler.getSummary);

commonRoutes.getCommonRoutes(router, handler, false);

module.exports = router;