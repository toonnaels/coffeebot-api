const express = require('express');
const passport = require('passport');
const router = express.Router();

const commonRoutes = require('./commonRoutes');
const handler = require('../handlers/userHandler');

router.post('/register', passport.authenticate('jwt', { session: true }), handler.registerUser);
router.post('/loadCredit', passport.authenticate('jwt', { session: true }), handler.loadCredit);

router.get('/topDrinkers', handler.getTopDrinkers);
router.get('/topMilk', handler.getTopMilk);
router.get('/topDebtors', handler.getTopDebtors);
router.get('/topPayers', handler.getTopPayers);

router.get('/studentNumber/:studentNumber', handler.getUserByStudentNumber);
router.get('/cardId/:cardId', handler.getUserByCardId);

router.get('/purchase/:userId/:item', handler.purchase);

commonRoutes.getCommonRoutes(router, handler, false);

module.exports = router;