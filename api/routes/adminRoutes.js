const express = require('express');
const router = express.Router();

const commonRoutes = require('./commonRoutes');
const handler = require('../handlers/adminHandler');

commonRoutes.getCommonRoutes(router, handler, true);

router.post('/authenticate', handler.authenticateAdmin);
router.post('/changePassword/:id', handler.changePassword);

module.exports = router;