// Imports.
const express = require('express');
const app = express();

const router = require('./router');

// Environmental variables.
const port = process.env.API_PORT || 3000;

// Starts the server.
function start() {
  // Add all the routes.
  router.routes(app);

  // Start listening on server.
  app.listen(port, () => {
    console.log(`The server has started.\nListening on port ${port}...`)
  });
}

exports.start = start