// Imports.
const cors = require('cors');
const passport = require('passport');
const passportStrategies = require('./passport-strategies');

const user = require('../routes/userRoutes');
const coffeeOption = require('../routes/coffeeOptionRoutes');
const purchase = require('../routes/purchaseRoutes');
const unregisteredUsers = require('../routes/unregisteredUserRoutes');
const admin = require('../routes/adminRoutes');
const activity = require('../routes/activityRoutes');

var bodyParser = require('body-parser');

// Acquire all routes.
function routes(app) {
  app.use(bodyParser.json()); // for parsing applicaiton/json
  app.use(bodyParser.urlencoded({ extended: true})); // for parsing application/x-www-form-urlencoded
  
  // CORS
  app.use(cors());

  // Session management.
  app.use(passport.initialize());
  app.use(passport.session());
  passportStrategies.jwtStrategy(passport);

  // API routes.
  app.use('/api/users', user);
  app.use('/api/coffeeOptions', coffeeOption);
  app.use('/api/purchases', purchase);
  app.use('/api/unregisteredUsers', unregisteredUsers);
  app.use('/api/admin', admin)
  app.use('/api/activities', activity)

  // Error handling middelware.
  function logError(err, req, res, next) {
    console.log(err);
    next(err);
  }

  function handleDefinedErrors(err, req, res, next) {
    if(err.status) {
      res.status(err.status).send(err);
    } else {
      next(err);
    }
  }

  function handleUndefinedErrors(err, req, res, next) {
    res.status(500).send("Internal Server Error")
  }

  app.use(logError);
  app.use(handleDefinedErrors);
  app.use(handleUndefinedErrors);
}

// Check if user is logged in.
function requireLogin (req, next, exec) {
  passport.authenticate('jwt', { session: true });
};

exports.routes = routes;
exports.requireLogin = requireLogin;