const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

const adminDao = require('../dao/adminDao');

const secret = '73738f77-e9b0-40a0-becc-d9a928e6ba3c';

function jwtStrategy(passport) {
    let opts = {};
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
    opts.secretOrKey = secret;

    passport.use(new JwtStrategy(opts, (jwt_payload, done) => {
        adminDao.getAdminByQuery({ email: jwt_payload.email, password: jwt_payload.password}, (err, user) => {
            if(err) {
                return done(err, false);
            }

            if(user) {
                return done(null, user);
            } else {
                return done(null, false);
            }
        });
    }));

    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });
    
    passport.deserializeUser(function(id, done) {
        adminDao.getAdminByQuery({ _id: id}, function(err, user) {
            done(err, user);
        });
    });
}

exports.secret = secret;
exports.jwtStrategy = jwtStrategy;