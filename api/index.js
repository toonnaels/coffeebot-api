const server = require("./server/server");
const db = require("./db/db");

const Activity = require('./db/activity');
const Admin = require('./db/admin');
const CoffeeOption = require('./db/coffeeOption');
const Purchase = require('./db/purchase');
const UnregisteredUser = require('./db/unregisteredUser');
const User = require('./db/user');

// Connect to database.
db.connect().then(_ => {

    // Sequential db execution
    Promise.resolve()
    // Sync tables.
    .then(_ => Activity.sync().then(_ => { console.log("Activity table synced."); }))
    .then(_ => Admin.sync().then(_ => { console.log("Admin table synced."); }))
    .then(_ => User.sync().then(_ => { console.log("User table synced."); }))
    .then(_ => CoffeeOption.sync().then(_ => { console.log("CoffeeOption table synced."); }))
    .then(_ => Purchase.sync().then(_ => { console.log("Purchase table synced."); }))
    .then(_ => UnregisteredUser.sync().then(_ => { console.log("UnregisteredUser table synced."); }));

    // Start server.
    server.start();
});